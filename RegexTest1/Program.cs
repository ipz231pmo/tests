﻿using System.Text;
using System.Text.RegularExpressions;

while (true)
{
    Console.OutputEncoding = Encoding.Unicode;
    Console.InputEncoding = Encoding.Unicode;

    string s = Console.ReadLine();
    
    Match match = Regex.Match(s, "^\\p{Lu}{1}\\p{Ll}{0,}\\s\\p{Lu}{1}\\.\\p{Lu}{1}\\.$");

    if (match.Success)
        Console.WriteLine("Success");
    else
        Console.WriteLine("Error");
}