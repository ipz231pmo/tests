﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mvc3.views
{
    public class ListForm : Form
    {
        public ListBox lbList;

        private void InitializeComponent()
        {
            this.lbList = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // lbList
            // 
            this.lbList.FormattingEnabled = true;
            this.lbList.Location = new System.Drawing.Point(12, 12);
            this.lbList.Name = "lbList";
            this.lbList.Size = new System.Drawing.Size(541, 329);
            this.lbList.TabIndex = 0;
            // 
            // ListForm
            // 
            this.ClientSize = new System.Drawing.Size(565, 353);
            this.Controls.Add(this.lbList);
            this.Name = "ListForm";
            this.ResumeLayout(false);

        }
        public ListForm()
        {
            InitializeComponent();
        }
    }
}
