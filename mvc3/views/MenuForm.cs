﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mvc3.views
{
    public class MenuForm : Form
    {
        public Button btnOpen;

        private void InitializeComponent()
        {
            this.btnOpen = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnOpen
            // 
            this.btnOpen.Location = new System.Drawing.Point(527, 296);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(242, 83);
            this.btnOpen.TabIndex = 0;
            this.btnOpen.Text = "Open Time List";
            this.btnOpen.UseVisualStyleBackColor = true;
            // 
            // MenuForm
            // 
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(1280, 720);
            this.Controls.Add(this.btnOpen);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MenuForm";
            this.ResumeLayout(false);

        }

        public MenuForm()
        {
            InitializeComponent();
        }


    }
}
