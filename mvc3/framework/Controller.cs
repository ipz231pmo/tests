﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TextBox;

namespace mvc3.framework
{
    public abstract class Controller : Subscriber<EventArgs>
    {
        public bool NeedViewUpdate { get; set; }
        public abstract Form Form { get; }
        protected System.Windows.Forms.Timer updateTimer;
        
        public Controller Parent { get; set; }
        List<Controller> childs = new List<Controller>();

        /// <summary>
        /// Add child Controller to this.
        /// </summary>
        /// <param name="controller">Child Controller</param>
        public void AddChild(Controller controller) 
        { 
            controller.Form.TopLevel = false;
            Form.Controls.Add(controller.Form);
            controller.Parent = this;
            childs.Add(controller); 
            controller.Form.Show();
            controller.Form.BringToFront();
        }
        /// <summary>
        /// Remove child Controller from this
        /// </summary>
        /// <param name="controller">Child Controller</param>
        public void RemoveChild(Controller controller)
        {
            Form.Controls.Remove(controller.Form);
            controller.Parent = null;
            childs.Remove(controller); 
        }

        /// <summary>        
        /// Need only for main Controller Initialization
        /// </summary>
        public Controller()
        {
            updateTimer = new System.Windows.Forms.Timer();
            updateTimer.Tick += Tick;
            updateTimer.Enabled = true;
        }
        /// <summary>
        /// Set NeedViewUpdate to false
        /// </summary>
        protected virtual void UpdateView() => NeedViewUpdate = false;
        protected virtual void Tick(object sender, EventArgs args) { }
        public virtual void SubscriberNotify(object sender, EventArgs args) => NeedViewUpdate = true;
    }
}