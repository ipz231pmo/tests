﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mvc3.framework
{
    public interface Publisher<T> where T : EventArgs
    {
        event EventHandler<T> SubscribeMethods;
    }
}
