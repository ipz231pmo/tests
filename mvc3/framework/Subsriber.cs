﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mvc3.framework
{
    public interface Subscriber<T> where T : EventArgs
    {
        void SubscriberNotify(object sender, T args);
    }
}
