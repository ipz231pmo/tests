﻿using mvc3.views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mvc3.controllers
{
    public class ListController : framework.Controller
    {
        public override Form Form
        {
            get => frm;
        }

        ListForm frm;

        public ListController()
        {
            frm = new ListForm();
            Data.Instance.SubscribeMethods += SubscriberNotify;
            frm.FormClosed += Close;
        }

        private void Close(object sender, FormClosedEventArgs e)
        {
            Parent.RemoveChild(this);
        }
        protected override void Tick(object sender, EventArgs args)
        {
            if(NeedViewUpdate) UpdateView();
        }
        protected override void UpdateView()
        {
            frm.lbList.Items.Clear();

            foreach(string s in Data.Instance.Times)
            {
                frm.lbList.Items.Add(s);
            }

            base.UpdateView();
        }
    }
}
