﻿using mvc3.framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mvc3.controllers
{
    public class MenuController : framework.Controller
    {
        views.MenuForm frm;
        public override Form Form 
        { 
            get => frm;
        }
        public MenuController()
        {
            frm = new views.MenuForm();
            frm.btnOpen.Click += onBtnOpenClick;
        }

        private void onBtnOpenClick(object sender, EventArgs e)
        {
            ListController controller = new ListController();
            AddChild(controller);
        }
    }
}
