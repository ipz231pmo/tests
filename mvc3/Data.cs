﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mvc3
{
    public class Data : framework.Publisher<EventArgs>
    {
        private static Data instance;
        public static Data Instance { 
            get 
            {
                if(instance == null)
                {
                    instance = new Data();
                }
                return instance; 
            } 
        }

        public event EventHandler<EventArgs> SubscribeMethods;
        public List<string> Times = new List<string>();
        System.Windows.Forms.Timer updateTimer;
        int elapsedTime = 0;
        private Data() 
        {
            updateTimer = new System.Windows.Forms.Timer();
            updateTimer.Tick += Tick;   
            updateTimer.Interval = 16;
            updateTimer.Enabled = true;
        }

        private void Tick(object sender, EventArgs e)
        {
            elapsedTime += updateTimer.Interval;
            if( elapsedTime > 300 )
            {
                elapsedTime -= 300;
                Times.Add(DateTime.Now.ToString());
                SubscribeMethods?.Invoke(null, null);
            }
        }
    }
}
