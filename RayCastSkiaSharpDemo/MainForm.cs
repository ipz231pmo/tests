﻿using SkiaSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RayCastSkiaSharpDemo
{
    public partial class MainForm : Form
    {
        const int
            W = 30,
            H = 30,
            TileSize = 64;
        const float
            FOVANGLE = (float)Math.PI / 2,
            FOVDISTANCE = 25,
            FOVSPEED = 0.1f;

        int x, y;


        float fov = 0;

        private void skglControl_PaintSurface(object sender, SkiaSharp.Views.Desktop.SKPaintGLSurfaceEventArgs e)
        {
            e.Surface.Canvas.Clear(SKColor.Parse("#fff"));
            for (int i = 0; i < Width; i++)
            {
                float angle = fov + ((float)i / Width * 2.0f - 1.0f) * FOVANGLE / 2;
                float cos_a = (float)Math.Cos(angle);
                float sin_a = (float)Math.Sin(angle);

                int d = 0;


                while (true)
                {
                    float dy = cos_a * d;
                    float dx = sin_a * d;

                    int tileX = (int)(x + dx) / TileSize;
                    int tileY = (int)(y + dy) / TileSize;

                    if (tileX < 0 || tileX >= W || tileY < 0 || tileY >= H)
                    {
                        break;
                    }

                    if (map[tileX, tileY] == '#')
                    {

                        float heightProjected = d * FOVDISTANCE / Height;

                        SKPoint p0 = new SKPoint(i, Height / 2 - heightProjected / 2);
                        SKPoint p1 = new SKPoint(i, Height / 2 + heightProjected / 2);
                        SKPaint paint = new SKPaint
                        {
                            IsAntialias = true,
                            StrokeWidth = 1,
                            Color = SKColor.Parse("#f00")
                        };
                        e.Surface.Canvas.DrawLine(p0, p1, paint);

                        break;
                    }
                    d++;
                }

            }
        }

        private void Update(object sender, EventArgs e)
        {

            skglControl.Invalidate();
            label1.Text = $"X: {x}, Y:{ y}";
            label2.Text = $"Fov: {fov}";
            if (keysPressed[Keys.Z]) 
                fov -= FOVSPEED;
            if (keysPressed[Keys.X]) 
                fov += FOVSPEED;

            if (keysPressed[Keys.W]) 
                y -= 1;
            if (keysPressed[Keys.S]) y -= 1;
            if (keysPressed[Keys.A]) x -= 1;
            if (keysPressed[Keys.D]) y += 1;
        }

        char[,] map = new char[H, W];
        Dictionary<Keys, bool> keysPressed = new Dictionary<Keys, bool>();
        public MainForm()
        {
            InitializeComponent();

            skglControl.Size = Size;

            foreach (Keys key in Enum.GetValues(typeof(Keys)))
            {
                keysPressed[key] = false;
            }
            x = W * TileSize / 2; y = H * TileSize / 2;

            for (int i = 0; i < H; i++)
            {
                for (int j = 0; j < W; j++)
                {
                    if (i == 0 || j == 0 || i == H - 1 || j == W - 1) map[i, j] = '#';
                    else
                        map[i, j] = ' ';
                }
            }
        }
        private void OnKeyUp(object sender, KeyEventArgs e)
        {
            keysPressed[e.KeyCode] = false;
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            keysPressed[e.KeyCode] = true;
        }
    }
}
