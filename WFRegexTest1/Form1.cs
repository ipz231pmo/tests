using System.Text.RegularExpressions;

namespace WFRegexTest1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string s = textBox1.Text;

            Match match = Regex.Match(s, "^\\p{Lu}{1}\\p{Ll}{0,}\\s\\p{Lu}{1}\\.\\p{Lu}{1}\\.$");

            if (match.Success)
                MessageBox.Show("Success");
            else
                MessageBox.Show("Error");
        }
    }
}
