﻿

internal class Program
{
    public static void ConsolePrint(object? obj, EventArgs args)
    {
        Console.WriteLine("Console Write");
    }
    public static void FilePrint(object? obj, EventArgs args)
    {
        Console.WriteLine("File Write");
    }
    static EventHandler handler {  get; set; }
    private static void Main(string[] args)
    {
        //handler += ConsolePrint;
        handler(null, new EventArgs());
    }
}