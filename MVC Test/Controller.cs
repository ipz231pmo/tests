﻿using MVC_Test.Controllers;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MVC_Test
{
    public abstract class Controller
    {
        public abstract Form frm { get; }
        protected abstract void UpdateView(object obj, EventArgs args);

        public static void Load<T>() where T : Controller
        {
            T controller = Activator.CreateInstance<T>();

            Thread th = new Thread(new ThreadStart(() => { Application.Run(controller.frm); }));
            th.SetApartmentState(ApartmentState.STA);
            th.Start();
        }
    }
}
