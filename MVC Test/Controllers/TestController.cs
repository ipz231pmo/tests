﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MVC_Test.Controllers
{
    public class TestController : Controller
    {
        Forms.TestForm frmMain;
        public TestController()
        {
            frmMain = new Forms.TestForm();
            Data.Instance.DataChanged += UpdateView;
            frmMain.btnSubmit.MouseClick += OnbtnSubmitMouseClick;
        }

        override protected void UpdateView(object sender, EventArgs e)
        {
            
        }

        private void OnbtnSubmitMouseClick(object sender, MouseEventArgs e)
        {
            if (frmMain.tbName.Text == string.Empty) 
            {
                MessageBox.Show("Please Enter your name", "Error", MessageBoxButtons.OK);
                return;
            }
            if (frmMain.chbIsShrimp.Checked == false)
            {
                MessageBox.Show("You are Shrimp!\nDon't deny it!", "Error", MessageBoxButtons.OK);
                return;
            }
            if(!Data.Instance.AddShrimp(frmMain.tbName.Text))
            {
                MessageBox.Show("Data already has this shrimp", "Error", MessageBoxButtons.OK);
                return;
            }
        }

        public override Form frm
        {
            get => frmMain;
        }
    }
}
