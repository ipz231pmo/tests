﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MVC_Test.Controllers
{
    public class DefaultController : Controller
    {
        override public Form frm { get => frmMain; }
        Forms.DefaultView frmMain;
        public DefaultController()
        {
            frmMain = new Forms.DefaultView();
            frmMain.btnList.MouseClick += OnBtnListDown;
            frmMain.btnTest.MouseClick += OnBtnTestDown;
            Data.Instance.DataChanged += UpdateView;
        }

        protected override void UpdateView(object sender, EventArgs e)
        {
            frmMain.btnList.Invoke((Action) delegate { frmMain.btnList.Enabled = true; });
        }

        private void OnBtnTestDown(object sender, MouseEventArgs e)
        {
            Load<TestController>();
        }

        private void OnBtnListDown(object sender, MouseEventArgs e)
        {
            Load<ListController>();
        }
    }
}
