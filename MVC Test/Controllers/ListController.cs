﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MVC_Test.Controllers
{
    public class ListController : Controller
    {
        Forms.ListForm frmMain;
        public ListController()
        {
            frmMain = new Forms.ListForm();
            Data.Instance.DataChanged += UpdateView;
            foreach (var str in Data.Instance.Shrimps)
            {
                frmMain.listShrimpsList.Items.Add(str);
            }
        }
        protected override void UpdateView(object sender, EventArgs e)
        {
            frmMain.Invoke(new Action( () => {
                                                 frmMain.listShrimpsList.Items.Clear();
                                                 foreach (var str in Data.Instance.Shrimps)
                                                 {
                                                    frmMain.listShrimpsList.Items.Add(str);
                                                 }
                                             }
                                      )
                          )
            ;
        }

        public override Form frm
        {
            get => frmMain;
        }
    }
}
