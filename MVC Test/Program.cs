﻿using MVC_Test.Controllers;
using System;
using System.Windows.Forms;

namespace MVC_Test
{
    internal static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Controller.Load<DefaultController>();
        }
    }
}