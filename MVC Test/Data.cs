﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVC_Test
{
    public class Data
    {
        public event EventHandler<EventArgs> DataChanged;
        private static Data instance;
        public static Data Instance
        {
            get
            {
                if(instance == null)
                    instance = new Data();
                return instance;
            }
        }
        private Data()
        {
            shrimpsNames = new List<string>();
        }

        public bool AddShrimp(string name)
        {
            bool find = false;
            for (int i = 0; i < shrimpsNames.Count; i++)
                if (shrimpsNames[i] == name) { find = true; break; }
            if (!find) 
            { 
                shrimpsNames.Add(name); 
                DataChanged?.Invoke(this, EventArgs.Empty);
            }
            return !find;
        }

        private List<string> shrimpsNames;
        public List<string> Shrimps { get => shrimpsNames; }
        
    }
}
