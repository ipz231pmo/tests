﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MVC_Test2.Controllers
{
    public class MenuController : Controller
    {
        Timer timer;

        public override Form form
        {
            get => frm;
        }
        Views.MenuForm frm;
        public MenuController()
        {
            frm = new Views.MenuForm();
            timer = new Timer();
            //timer.Tick += Tick;
            timer.Start();
        }
        public override void Tick(object sender, EventArgs args)
        {
            frm.lblCurrentTime.Text = DateTime.Now.ToString();
        }
    }
}
