﻿using System;
using System.Windows.Forms;

namespace MVC_Test2.Controllers
{
    public abstract class Controller
    {
        public abstract Form form { get; }
        public abstract void Tick(Object sender, EventArgs args);
    }
}
