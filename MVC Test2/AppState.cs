﻿

using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace MVC_Test2
{
    public class AppState
    {
        private static AppState instance;
        public static AppState Instance
        {
            get
            {
                if(instance == null) instance = new AppState();
                return instance;
            }
        }
        public Form frm;
        public List<Controllers.Controller> controllers = new List<Controllers.Controller> ();
        System.Windows.Forms.Timer timer;
        public void Init()
        {
            instance.frm = new Form();
            instance.frm.Size = new System.Drawing.Size(1280, 720);

            instance.timer = new System.Windows.Forms.Timer();
            instance.timer.Interval = 16;
            instance.timer.Tick += Update;
            instance.timer.Start();

            AppState.Run<Controllers.MenuController>();

        }

        private void Update(object sender, EventArgs e)
        {
            foreach(Controllers.Controller controller in instance.controllers)
            {
                controller.Tick(null, null);
            }
        }

        public static void Run<T> () where T : Controllers.Controller 
        {
            T controller = Activator.CreateInstance<T>();
            if(controller ==  null)
            {
                throw new Exception("Null controller on create");
            }

            Instance.controllers.Add(controller);

            controller.form.TopLevel = false;
            Instance.frm.Controls.Add(controller.form);
            controller.form.Show();
            controller.form.BringToFront();
        }
    }
}
