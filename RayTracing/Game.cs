﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace RayTracing
{
    public class Game
    {
        const int W = 120, H = 30;
        const float ScreenAspect = (float)W / H;
        const float PixelAspect = (float)8 / 16;
        char[] pixelMap = new char[W * H];
        float f = 0.0f;
        System.Timers.Timer timer;

        public Game() 
        {
            timer = new System.Timers.Timer();
            timer.Elapsed += Draw;
            timer.Interval = 16;
            timer.Start();
        }

        private void Draw(object? sender, ElapsedEventArgs e)
        {
            //f += 16;
            for (int i = 0; i < W; i++)
            {
                for (int j = 0; j < H; j++)
                {
                    float x = (float)i / W * 2.0f - 1.0f;
                    float y = (float)j / H * 2.0f - 1.0f;
                    x *= ScreenAspect * PixelAspect;
                    //x += (float)Math.Sin(f * 0.01);
                    pixelMap[j * W + i] = ' ';
                    if (x * x + y * y < 0.5f) pixelMap[j * W + i] = (char)0x2588;
                    Console.SetCursorPosition(i, j);
                    Console.Write(pixelMap[j * W + i]);
                }
            }
        }

        public void Run()
        {
            while (true){}
        }
    }
}
