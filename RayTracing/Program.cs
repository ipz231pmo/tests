﻿using RayTracing;
using System.Globalization;
using System.Numerics;
using System.Text;

internal class Program
{
    
    private static void Main(string[] args)
    {
        Game game = new Game();
        game.Run();
    }
}