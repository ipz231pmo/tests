﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormsCameraTest1
{
    public class Tile
    {
        public const int TILE_SIZE = 42;
        
        private int x, y;
        private TileType type;

        public int X { get => x; }
        public int Y { get => y; }
        public TileType Type { get => type; }

        public Tile(int x, int y, TileType type)
        {
            this.x = x;
            this.y = y;
            this.type = type;            
        }

        public enum TileType
        {
            Grass,
            Dirt,
            Stone,
            Wood
        }
    }
}
