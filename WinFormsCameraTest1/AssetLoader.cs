﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormsCameraTest1
{
    public class AssetLoader
    {
        public static Bitmap GrassTexture 
        { 
            get
            {
                if (grassTexture == null)
                    throw new Exception("Nullable grassTexture object");
                return grassTexture;
            } 
        }
        public static Bitmap DirtTexture
        {
            get
            {
                if (dirtTexture == null)
                    throw new Exception("Nullable dirtTexture object");
                return dirtTexture;
            }
        }
        public static Bitmap StoneTexture
        {
            get
            {
                if (stoneTexture == null)
                    throw new Exception("Nullable stoneTexture object");
                return stoneTexture;
            }
        }
        public static Bitmap WoodTexture
        {
            get
            {
                if (woodTexture == null)
                    throw new Exception("Nullable woodTexture object");
                return woodTexture;
            }
        }

        public static Bitmap? grassTexture;
        public static Bitmap? dirtTexture;
        public static Bitmap? stoneTexture;
        public static Bitmap? woodTexture;

        public static void LoadTextures()
        {
            Bitmap tiles = Properties.Resources.Tiles;

            grassTexture = tiles.Clone(new Rectangle(0 * 42, 0, 42, 42), tiles.PixelFormat );
            dirtTexture =  tiles.Clone(new Rectangle(1 * 42, 0, 42, 42), tiles.PixelFormat );
            stoneTexture = tiles.Clone(new Rectangle(2 * 42, 0, 42, 42), tiles.PixelFormat );
            woodTexture =  tiles.Clone(new Rectangle(3 * 42, 0, 42, 42), tiles.PixelFormat );

        }
    }
}
