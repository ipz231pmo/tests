using SkiaSharp;
using SkiaSharp.Views.Desktop;

namespace WinFormsCameraTest1
{
    public partial class Form1 : Form
    {

        float CameraX=0, CameraY=0, moveSpeed = 4, CameraWidth = 800, CameraHeight = 600;

        List<Tile> map;
        Dictionary<Keys, bool> keysPressed;
        public Form1()
        {
            InitializeComponent();
            map = [];

            keysPressed = [];
            foreach (Keys key in Enum.GetValues(typeof(Keys))) keysPressed[key] = false;

            for (int i = 0; i < 50; i++)
            {
                for (int j = 0; j < 50; j++)
                {
                    Random random = new Random();
                    Tile tile = new Tile(i, j, (Tile.TileType)(random.Next() % 4));
                    map.Add(tile);
                }
            }
            skglControl1.Size = new Size((int)CameraWidth, (int)CameraHeight);

        }
        private void Update(object sender, EventArgs e)
        {
            skglControl1.Invalidate();
            if (keysPressed[Keys.D])
            {
                CameraX += moveSpeed;
            }
            if (keysPressed[Keys.A])
            {
                CameraX -= moveSpeed;
            }
            if (keysPressed[Keys.W])
            {
                CameraY += moveSpeed;
            }
            if (keysPressed[Keys.S])
            {
                CameraY -= moveSpeed;
            }

        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            keysPressed[e.KeyCode] = true;
        }

        private void OnKeyUp(object sender, KeyEventArgs e)
        {
            keysPressed[e.KeyCode] = false;

        }

        private void skglControl1_PaintSurface(object sender, SKPaintGLSurfaceEventArgs e)
        {
            e.Surface.Canvas.Clear(SKColor.Parse("#00f"));
            foreach (Tile tile in map)
            {

                float TileLeft = tile.X * Tile.TILE_SIZE - CameraX;
                float TileRight = tile.X * Tile.TILE_SIZE + Tile.TILE_SIZE - CameraX;

                float TileTop = CameraY + tile.Y * Tile.TILE_SIZE;
                float TileBottom = CameraY + tile.Y * Tile.TILE_SIZE + Tile.TILE_SIZE;

                if (TileRight >= 0 && TileLeft <= skglControl1.Width && TileTop <= skglControl1.Height && TileBottom >= 0)
                {
                    SKImage image;
                    switch(tile.Type)
                    {
                        case Tile.TileType.Grass:                            
                            image = AssetLoader.GrassTexture.ToSKImage();
                            break;
                        case Tile.TileType.Dirt:
                            image = AssetLoader.DirtTexture.ToSKImage();
                            break;
                        case Tile.TileType.Stone:
                            image = AssetLoader.StoneTexture.ToSKImage();
                            break;
                        case Tile.TileType.Wood:
                            image = AssetLoader.WoodTexture.ToSKImage();
                            break;
                        default:
                            throw new Exception("Error");
                    }
                    
                    SKRect dest = new SKRect(TileLeft, TileTop, TileRight, TileBottom);
                    e.Surface.Canvas.DrawImage(image,dest);

                }
            }
        }
    }
}
