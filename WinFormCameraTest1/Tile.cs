﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormCameraTest1
{
    public class Tile : PictureBox
    {
        public const int TILE_SIZE = 32;
        int x, y;
        public enum TileType
        {
            Grass,
            Dirt,
            Stone,
            Wood
        }
        public TileType Type { get; set; }
        public Tile(TileType type, int x, int y) 
        {
            this.Type = type;
        }
    }
}
