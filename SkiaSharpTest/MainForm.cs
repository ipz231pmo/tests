using SkiaSharp;

namespace SkiaSharpTest
{
    public partial class MainForm : Form
    {
        int x, y;
        public MainForm()
        {
            InitializeComponent();
            Canvas.Size = Size;
            x = Width / 2;
            y = Height / 2;
        }

        private void Canvas_PaintSurface(object sender, SkiaSharp.Views.Desktop.SKPaintGLSurfaceEventArgs e)
        {
            e.Surface.Canvas.Clear(SKColor.Parse("#fff"));
            var paint = new SKPaint
            {
                Color = new SKColor(255, 0, 0),
                StrokeWidth = 1,
                IsAntialias = true
            };

            e.Surface.Canvas.DrawCircle(x, y, 150.0f, paint);
        }

        private void Update(object sender, EventArgs e)
        {
            x += 1;
            Canvas.Invalidate();
        }
    }
}
