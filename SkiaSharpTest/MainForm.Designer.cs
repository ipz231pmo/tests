﻿namespace SkiaSharpTest
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            Canvas = new SkiaSharp.Views.Desktop.SKGLControl();
            timer = new System.Windows.Forms.Timer(components);
            SuspendLayout();
            // 
            // Canvas
            // 
            Canvas.BackColor = Color.Black;
            Canvas.Location = new Point(0, 0);
            Canvas.Margin = new Padding(4, 3, 4, 3);
            Canvas.Name = "Canvas";
            Canvas.Size = new Size(726, 405);
            Canvas.TabIndex = 0;
            Canvas.VSync = true;
            Canvas.PaintSurface += Canvas_PaintSurface;
            // 
            // timer
            // 
            timer.Enabled = true;
            timer.Interval = 1;
            timer.Tick += Update;
            // 
            // MainForm
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(Canvas);
            Name = "MainForm";
            Text = "Form1";
            ResumeLayout(false);
        }

        #endregion

        private SkiaSharp.Views.Desktop.SKGLControl Canvas;
        private System.Windows.Forms.Timer timer;
    }
}
